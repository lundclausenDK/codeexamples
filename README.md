# Code Examples
A handful of code snippets for combined showcase.

My mission:

1. Delivering useful programs and features to end-users
2. Constantly improving my code quality
3. Repeat from step 1

By Mikkel Lund Clausen  
https://www.linkedin.com/in/mikkel-lund-clausen-a7403452/
